/*
 * Copyright (c) 2022 SEBASTIAN BEDIN <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : task_blinking.c
 * @date   : May 17, 2022
 * @author : SEBASTIAN BEDIN <sebabedin@gmail.com>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "arch.h"
#include "timers.h"

#include "uppercase.h"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

static uppercase_t *rx_uppercase_;
//static uppercase_t *uppercase_tx_;

/********************** external data definition *****************************/

extern xQueueHandle tx_queue_;
extern xQueueHandle rx_queue_;

/********************** internal functions definition ************************/

static uppercase_t* create_(void)
{
  uppercase_t *uppercase;

  uppercase = (uppercase_t*)pvPortMalloc(sizeof(uppercase_t));
  if (NULL == uppercase)
  {
    return NULL;
  }

  uppercase->data = NULL;
  uppercase->size = 0;

  uppercase->data = (char*)pvPortMalloc(sizeof(char) * UPPERCASE_MESSAGE_MAX_SIZE);
  if (NULL == uppercase->data)
  {
    vPortFree((void*)uppercase);
    return NULL;
  }

  return uppercase;
}

static uppercase_t* destroid_(uppercase_t *uppercase)
{
  if (NULL == uppercase)
  {
    return;
  }

  vPortFree((void*)uppercase->data);
  vPortFree((void*)uppercase);

  return NULL;
}

/********************** external functions definition ************************/

void task_uppercase_uart_rx_user_cb(void)
{
  BaseType_t xHigherPriorityTaskWoken = pdFALSE;

  {
    uint8_t byte;
    while (0 < uart_driver_read(&byte, 1))
    {
      if (true == uppercase_letter_add(rx_uppercase_, byte))
      {
        portBASE_TYPE status;
        status = xQueueSendToBackFromISR(tx_queue_, (void* )&rx_uppercase_, &xHigherPriorityTaskWoken);
        if (pdPASS == status)
        {
          rx_uppercase_ = NULL;
          xQueueReceiveFromISR(rx_queue_, (void*)&rx_uppercase_, &xHigherPriorityTaskWoken);
          uppercase_init(rx_uppercase_);
        }
        else
        {
          // Si la cola está llena descarto el mensaje
          uppercase_init(rx_uppercase_);
        }
      }
    }
  }

  portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

void task_uppercase(void *pvParameters)
{
  portBASE_TYPE status;

  do
  {
    rx_uppercase_ = create_();
    if (NULL != rx_uppercase_)
    {
      uppercase_init(rx_uppercase_);
      status = xQueueSendToBack(rx_queue_, (void* )&rx_uppercase_, 0);
    }
  } while ((NULL != rx_uppercase_) && (pdPASS != status));

  rx_uppercase_ = create_();
  if (NULL != rx_uppercase_)
  {
    uppercase_init(rx_uppercase_);
  }

  while (true)
  {
    {
      uppercase_t *tx_uppercase;

      status = xQueueReceive(tx_queue_, (void*)&tx_uppercase, portMAX_DELAY);
      if (pdPASS == status)
      {
        uppercase_process_message(tx_uppercase);

        char *data = uppercase_get_data(tx_uppercase);
        uint16_t size = uppercase_get_size(tx_uppercase);

        uint16_t sended_bytes = 0;
        while (0 < (size - sended_bytes))
        {
          sended_bytes += uart_driver_write((uint8_t*)(data + sended_bytes), (size - sended_bytes));
        }

        tx_uppercase = destroid_(tx_uppercase);
      }
    }

    {
      uppercase_t *new_uppercase;
      do
      {
        new_uppercase = create_();
        if (NULL != new_uppercase)
        {
          uppercase_init(new_uppercase);
          status = xQueueSendToBack(rx_queue_, (void* )&new_uppercase, 0);
        }
      } while ((NULL != new_uppercase) && (pdPASS != status));
    }
  }
}

/********************** end of file ******************************************/
