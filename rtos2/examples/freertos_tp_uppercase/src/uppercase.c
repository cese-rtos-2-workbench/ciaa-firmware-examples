/*
 * Copyright (c) YEAR NOMBRE <MAIL>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : uppercase.c
 * @date   : Jun 6, 2022
 * @author : NOMBRE <MAIL>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "arch.h"
#include "uppercase.h"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

static uppercase_t* create_(void)
{
  uppercase_t *uppercase;

  uppercase = (uppercase_t*)pvPortMalloc(sizeof(uppercase_t));
  if (NULL == uppercase)
  {
    return NULL;
  }

  uppercase->data = NULL;
  uppercase->size = 0;

  uppercase->data = (char*)pvPortMalloc(sizeof(char) * UPPERCASE_MESSAGE_MAX_SIZE);
  if (NULL == uppercase->data)
  {
    vPortFree((void*)uppercase);
    return NULL;
  }

  return uppercase;
}

static bool is_letter_valid_(uint8_t letter)
{
  if (('a' <= letter && letter <= 'z') || ('0' <= letter && letter <= '9'))
  {
    return true;
  }
  return false;
}

static void letter_add_(uppercase_t *uppercase, uint8_t letter)
{
  if (uppercase->size < UPPERCASE_MESSAGE_MAX_SIZE)
  {
    uppercase->data[uppercase->size] = letter;
    uppercase->size++;
  }
}

static bool is_end_of_line_(uint8_t letter)
{
  if (('\n' == letter) || ('\r' == letter))
  {
    return true;
  }
  return false;
}

/********************** external functions definition ************************/

void uppercase_init(uppercase_t *uppercase)
{
  if (NULL == uppercase)
  {
    return;
  }

  uppercase->size = 0;
}

uppercase_t* uppercase_create(void)
{
  uppercase_t *uppercase = create_();
  uppercase_init(uppercase);
  return uppercase;
}

uppercase_t* uppercase_destroid(uppercase_t *uppercase)
{
  if (NULL == uppercase)
  {
    return;
  }

  vPortFree((void*)uppercase->data);
  vPortFree((void*)uppercase);

  return NULL;
}

bool uppercase_letter_add(uppercase_t *uppercase, uint8_t letter)
{
  if (NULL == uppercase)
  {
    return false;
  }

  if (true == is_letter_valid_(letter))
  {
    letter_add_(uppercase, letter);
  }
  else if (true == is_end_of_line_(letter))
  {
    return true;
  }
  return false;
}

void uppercase_process_message(uppercase_t *uppercase)
{
  for (uint16_t i = 0; i < uppercase->size; ++i)
  {
    uint8_t letter = uppercase->data[i];
    if ('a' <= letter && letter <= 'z')
    {
      letter = letter - ('a' - 'A');
      uppercase->data[i] = letter;
    }
  }
}

char* uppercase_get_data(uppercase_t *uppercase)
{
  return uppercase->data;
}

uint16_t uppercase_get_size(uppercase_t *uppercase)
{
  return uppercase->size;
}

/********************** end of file ******************************************/
